﻿using Deficienteable.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace Deficienteable.API.Data.Context
{
    public class DeficienteableContext : DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DeficienteableContext(DbContextOptions<DeficienteableContext> options)
            : base(options) { }

    }
}
