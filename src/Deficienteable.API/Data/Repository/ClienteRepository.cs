﻿using Deficienteable.API.Data.Context;
using Deficienteable.API.Data.Repository.Interfaces;
using Deficienteable.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Deficienteable.API.Data.Repository
{
    public class ClienteRepository : Repository<Cliente>, IClienteRepository
    {
        public ClienteRepository(DeficienteableContext context) : base(context)
        {
        }

        public virtual Cliente GetClienteId(Guid id)
        {
            var result = _context.Clientes
                .SingleOrDefault(c => c.ClienteId == id);
            return result;
        }
        public IEnumerable<Cliente> GetClientes()
        {
            return Get().ToList();
        }

    }
}
