﻿using Deficienteable.API.Entities;
using System;
using System.Collections.Generic;

namespace Deficienteable.API.Data.Repository.Interfaces
{
    public interface IClienteRepository : IRepository<Cliente>
    {
        IEnumerable<Cliente> GetClientes();
        Cliente GetClienteId(Guid id);
    }
}
