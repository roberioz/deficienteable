﻿using Deficienteable.API.Data.Context;
using Deficienteable.API.Data.Repository;
using Deficienteable.API.Data.Repository.Interfaces;
using Deficienteable.API.Data.UoW.Interfaces;

namespace Deficienteable.API.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private ClienteRepository _clienteRepository;
        public DeficienteableContext _context;

        public UnitOfWork(DeficienteableContext context)
        {
            _context = context;
        }

        public IClienteRepository ClienteRepository
        {
            get
            {
                return _clienteRepository ??= new ClienteRepository(_context);
            }
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
