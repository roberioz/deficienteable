﻿using Deficienteable.API.Data.Repository.Interfaces;

namespace Deficienteable.API.Data.UoW.Interfaces
{
    public interface IUnitOfWork
    {
        IClienteRepository ClienteRepository { get; }

        void Commit();
    }
}
