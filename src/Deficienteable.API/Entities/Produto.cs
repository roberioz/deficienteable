﻿using System;

namespace Deficienteable.API.Entities
{
    public class Produto
    {
        public Produto(Guid produtoId)
        {
            ProdutoId = produtoId;
        }

        public Guid ProdutoId { get; set; }
        public string Nome { get; set; }
        public string Quantidade { get; set; }
    }
}
