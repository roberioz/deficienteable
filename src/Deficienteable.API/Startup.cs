using Deficienteable.API.Data.Context;
using Deficienteable.API.Data.UoW;
using Deficienteable.API.Data.UoW.Interfaces;
using Deficienteable.API.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Deficienteable.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //CORS       
            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });

            //Unit Of Work
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            //ContextDB
            services.AddDbContext<DeficienteableContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Deficienteable.API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //CORS
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Deficienteable.API v1"));
            }
            //Seed Method
            PopularBanco(app);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        //Seed Method
        private void PopularBanco(IApplicationBuilder app)
        {
            using var context = app.ApplicationServices
                .CreateScope()
                .ServiceProvider
                .GetRequiredService<DeficienteableContext>();
            if (context.Database.EnsureCreated())
            {
                context.Clientes.Add(new Cliente
                {
                    Nome = "Roberio Silva",
                    Email = "roberio.pb@hotmail.com",
                    CPF = "12345678901",
                    Telefone = "62995117080",
                    CEP = "75095690",
                    Logradouro = "Rua 10",
                    Numero = 11,
                    Bairro = "Lourdes",
                    Cidade = "Anapolis",
                    Estado = "GO"

                });
                context.SaveChanges();
            }
        }
}
