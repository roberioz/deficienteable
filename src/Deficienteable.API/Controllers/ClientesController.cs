﻿using Deficienteable.API.Data.UoW.Interfaces;
using Deficienteable.API.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Deficienteable.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly IUnitOfWork _uof;

        public ClientesController(IUnitOfWork context)
        {
            _uof = context;
        }

        //HTTP GET
        [HttpGet]
        public ActionResult<IEnumerable<Cliente>> GetClientes()
        {
            return _uof.ClienteRepository.GetClientes().ToList();
        }
        [HttpGet("{id}", Name = "GetCliente")]
        public ActionResult<Cliente> GetClienteId(Guid id)
        {
            var cliente = _uof.ClienteRepository.GetClienteId(id);

            if (cliente == null)
            {
                return NotFound($"Cliente com id={id} não encontrado");
            }
            return cliente;
        }

        //HTTP POST
        [HttpPost]
        public ActionResult PostCliente([FromBody] Cliente cliente)
        {
            _uof.ClienteRepository.Add(cliente);
            _uof.Commit();

            return new CreatedAtRouteResult("GetCliente",
            new { id = cliente.ClienteId }, cliente);
        }

        //HTTP PUT
        [HttpPut("{id}")]
        public ActionResult PutCliente(Guid? id, [FromBody] Cliente cliente)
        {
            if (id != cliente.ClienteId)
            {
                return BadRequest($"Não foi possivel atualizar o cliente com id={id}.");
            }
            _uof.ClienteRepository.Update(cliente);
            _uof.Commit();
            return Ok("Cliente atualizado com sucesso.");
        }

        //HTTP DELETE
        [HttpDelete("{id}")]
        public ActionResult DeleteCliente(Guid? id)
        {
            var cliente = _uof.ClienteRepository.GetById(c => c.ClienteId == id);

            if (cliente == null)
            {
                return NotFound($"Não foi possivel encontrar o cliente com id={id}.");
            }

            _uof.ClienteRepository.Delete(cliente);
            _uof.Commit();

            return NoContent();

        }
    }
}
